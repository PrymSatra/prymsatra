﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;



public class SoundManager : MonoBehaviour
{

    [SerializeField] private SoundClip[] soundClips;
    [SerializeField] private AudioSource audioSource;

    public static SoundManager Instance { get; private set; }

    [Serializable]
    public struct SoundClip
    {
        public Sound Sound;
        public AudioClip AudioClip;
    }

    public enum Sound
    {
        BGM,
        Fire,
        Boom,
    }



    /// <summary>
    /// play sound
    /// </summary>
    /// <param name="audioSource"></param>
    /// <param name="sound"></param>
    public void Play(AudioSource audioSource, Sound sound)
    {
        Debug.Assert(audioSource != null, "audioSource cannot be null");

        audioSource.clip = GetAudioClip(sound);
        audioSource.Play();
    }

    public void PlayBGM()
    {
        audioSource.loop = true;
        Play(audioSource, Sound.BGM);
    }


    private AudioClip GetAudioClip(Sound sound)
    {
        foreach (var soundClip in soundClips)
        {
            if (soundClip.Sound == sound)
            {
                return soundClip.AudioClip;
            }
        }
        Debug.Assert(false, $"Cannot find Sound {sound}");
        return null;
    }

    private void Awake()
    {
        Debug.Assert(audioSource != null, "audioSource cannot be null");
        Debug.Assert(soundClips != null, "audioSource cannot be null");

        if (Instance == null)
        {
            Instance = this;
        }
        DontDestroyOnLoad(this);
    }
}

